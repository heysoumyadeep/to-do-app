package com.appsofsoumyadeep.notepad.Model;

public class Task {
    private String taskTitle, taskDesc;
    public Task(String taskTitle, String taskDescription) {
        this.taskTitle = taskTitle;
        this.taskDesc = taskDescription;
    }

    public Task() {
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDescription) {
        this.taskDesc = taskDescription;
    }
}
