package com.appsofsoumyadeep.notepad.MainActivity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appsofsoumyadeep.notepad.R;
import com.appsofsoumyadeep.notepad.RegisterScreen.RegisterScreen;
import com.appsofsoumyadeep.notepad.Model.User;
import com.appsofsoumyadeep.notepad.UI.UserRecyclerViewAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class UserList extends Fragment {
    private DatabaseReference mDataBaseReference;
    private RecyclerView recyclerView;
    private UserRecyclerViewAdapter userRecyclerViewAdapter;
    private List<User> userList;
    private FirebaseUser mUser;
    private FirebaseAuth mAuth;
    private ImageButton dropdown;
    private ConstraintLayout expandableView;
    private LinearLayout FAB, logoutButton, viewTasks;
    private CardView userInfo;
    private TextView userName, userAge, userDOB;

    public UserList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_user_list, container, false);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        mDataBaseReference = FirebaseDatabase.getInstance().getReference().child("Users");
        mDataBaseReference.keepSynced(true);

        userName = view.findViewById(R.id.userName);
        userAge = view.findViewById(R.id.userAge);
        userDOB = view.findViewById(R.id.userDOB);

        userList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.userList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        dropdown = view.findViewById(R.id.dropdown_menu);
        FAB = view.findViewById(R.id.floatingActionButton);
        viewTasks = view.findViewById(R.id.viewTasks);
        expandableView = view.findViewById(R.id.dropdownLIST);
        logoutButton = view.findViewById(R.id.logoutButton);
        userInfo = view.findViewById(R.id.userInfo);

        dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandableView.getVisibility()==View.GONE){
                    TransitionManager.beginDelayedTransition(userInfo, new AutoTransition());
                    expandableView.setVisibility(View.VISIBLE);
                    dropdown.setBackgroundResource(R.drawable.ic_baseline_arrow_drop_up_24);
                } else {
                    expandableView.setVisibility(View.GONE);
                    dropdown.setBackgroundResource(R.drawable.ic_baseline_arrow_drop_down_24);
                }
            }
        });

        FAB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_userList_to_addUserTask);
            }
        });

        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("userID", mUser.getUid());
                Navigation.findNavController(v).navigate(R.id.action_userList_to_userTasks,bundle);
            }
        });

        viewTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("userID", mUser.getUid());
                Navigation.findNavController(v).navigate(R.id.action_userList_to_userTasks,bundle);
            }
        });


        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mUser != null && mAuth != null){
                    mAuth.signOut();
                    Toast.makeText(getContext(), "You have Successfully Logged Out !",
                            Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), RegisterScreen.class));
                    getActivity().finish();
                }
            }
        });

        return view;
    }

    private void setCurrentUserInfo(User user) {
        userName.setText("Name: "+user.getUserName()+" (You)");
        userAge.setText("Age: "+user.getUserAge());
        userDOB.setText("DOB: "+user.getUserDOB());
    }

    @Override
    public void onStart() {
        super.onStart();

        mDataBaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                userList.clear();
                for(DataSnapshot dataSnapshot: snapshot.getChildren()){
                    User user = dataSnapshot.getValue(User.class);
//                    Log.d("GG", "GG: "+user.getUserID());
                    if(user.getUserID().equals(mUser.getUid().toString()))
                        setCurrentUserInfo(user);
                    else
                        userList.add(user);
                    }

                userRecyclerViewAdapter = new UserRecyclerViewAdapter(getContext(),userList);
                recyclerView.setAdapter(userRecyclerViewAdapter);
                userRecyclerViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        inflater.inflate(R.menu.theme_mode, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull @NotNull MenuItem menu) {
        switch (menu.getItemId()) {
            case R.id.themeMode:{
                    if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    else
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            default: return super.onOptionsItemSelected(menu);
        }
    }
}