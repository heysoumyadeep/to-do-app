package com.appsofsoumyadeep.notepad;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.appsofsoumyadeep.notepad.RegisterScreen.RegisterScreen;

public class SplashScreen extends AppCompatActivity {

    private TextView devName, appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        devName = findViewById(R.id.developerName);
        appName = findViewById(R.id.appName);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, RegisterScreen.class));
                finish();
            }
        }, 1000);
    }

}