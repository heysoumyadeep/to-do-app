package com.appsofsoumyadeep.notepad.Model;

public class User {
    String userName, userAge, userDOB, userID;
    Boolean taskStatus;

    public User() {
    }

    public User(String userName, String userAge, String userDOB, String userID) {
        this.userName = userName;
        this.userAge = userAge;
        this.userDOB = userDOB;
        this.userID = userID;
    }

    public User(String userName, String userAge, String userDOB, String userID, Boolean taskStatus) {
        this.userName = userName;
        this.userAge = userAge;
        this.userDOB = userDOB;
        this.userID = userID;
        this.taskStatus = taskStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAge() {
        return userAge;
    }

    public void setUserAge(String userAge) {
        this.userAge = userAge;
    }

    public String getUserDOB() {
        return userDOB;
    }

    public void setUserDOB(String userDOB) {
        this.userDOB = userDOB;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Boolean getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Boolean taskStatus) {
        this.taskStatus = taskStatus;
    }
}
