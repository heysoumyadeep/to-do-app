package com.appsofsoumyadeep.notepad.MainActivity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appsofsoumyadeep.notepad.R;
import com.appsofsoumyadeep.notepad.Model.Task;
import com.appsofsoumyadeep.notepad.UI.TaskRecyclerViewAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class UserTasks extends Fragment {

    private String userID;
    private RecyclerView recyclerView;
    private TaskRecyclerViewAdapter recyclerViewAdapter;
    private List<Task> taskList;
    private DatabaseReference mDataBase;
    private TextView noTaskText;

    public UserTasks() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_tasks, container, false);
        userID = getArguments().getString("userID");

        mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").
                child(userID).child("Tasks");
        mDataBase.keepSynced(true);

        noTaskText = (TextView) view.findViewById(R.id.noTaskText);

        taskList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.taskList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                taskList.clear();
                for(DataSnapshot dataSnapshot: snapshot.getChildren()){
                    Task task = dataSnapshot.getValue(Task.class);
                    taskList.add(task);
                }

                if(taskList.isEmpty())
                    noTaskText.setVisibility(View.VISIBLE);

                recyclerViewAdapter = new TaskRecyclerViewAdapter(getContext(),taskList);
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
    }
}