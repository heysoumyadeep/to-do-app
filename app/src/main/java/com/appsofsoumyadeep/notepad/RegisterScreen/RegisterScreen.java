package com.appsofsoumyadeep.notepad.RegisterScreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import android.os.Bundle;

import com.appsofsoumyadeep.notepad.R;

public class RegisterScreen extends AppCompatActivity {
    private NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);

        navController  = Navigation.findNavController(this,R.id.createAccountActivity);
        NavigationUI.setupActionBarWithNavController(this,navController);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

    }

    @Override
    public boolean onSupportNavigateUp() {
        return navController.navigateUp();
    }

}