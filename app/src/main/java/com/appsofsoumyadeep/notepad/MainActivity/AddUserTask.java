package com.appsofsoumyadeep.notepad.MainActivity;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.appsofsoumyadeep.notepad.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class AddUserTask extends Fragment {
    public AddUserTask() {
    }

    private EditText mTaskTitle, mTaskDesc;
    private Button mAssignButton;
    private DatabaseReference mDataBase;
    private FirebaseUser mUser;
    private FirebaseAuth mAuth;
    private ProgressDialog mProgress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_user_task, container, false);

        mProgress = new ProgressDialog(getContext());

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(mUser.getUid());
        mDataBase.keepSynced(true);

        mTaskTitle = (EditText) view.findViewById(R.id.taskTitleET);
        mTaskDesc = (EditText) view.findViewById(R.id.taskDescET);
        mAssignButton = (Button) view.findViewById(R.id.assignButton);

        mAssignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTask();
                Navigation.findNavController(view).navigateUp();
            }
        });

        return view;
    }

    private void addTask() {
        mProgress.setMessage("Adding Task");
        mProgress.show();
//        Log.d("GG", "onChildAdded: "+mUser.getUid());
        String taskTitle = mTaskTitle.getText().toString().trim();
        String taskDesc = mTaskDesc.getText().toString().trim();

        if(!TextUtils.isEmpty(taskTitle) && !TextUtils.isEmpty(taskDesc)){
//            Log.d("GG", "SADF: "+mUser.getDisplayName());
            Map<String,Object> dataToSave = new HashMap<>();
            dataToSave.put("taskTitle",taskTitle);
            dataToSave.put("taskDesc",taskDesc);
            mDataBase.child("Tasks").push().setValue(dataToSave);
            mProgress.dismiss();

        }
    }
}