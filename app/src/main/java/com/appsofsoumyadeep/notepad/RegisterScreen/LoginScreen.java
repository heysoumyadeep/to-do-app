package com.appsofsoumyadeep.notepad.RegisterScreen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appsofsoumyadeep.notepad.MainActivity.MainActivity;
import com.appsofsoumyadeep.notepad.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.jetbrains.annotations.NotNull;

public class LoginScreen extends Fragment {

    private FirebaseAuth mAuth;
    private Button login;
    private EditText emailField;
    private EditText passwordField;
    private ProgressDialog progressDialog;

    public LoginScreen() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_login_activity, container, false);

        mAuth = FirebaseAuth.getInstance();

        login = view.findViewById(R.id.loginButton);
        emailField = view.findViewById(R.id.inputEmaillogin);
        passwordField = view.findViewById(R.id.inputPasswordlogin);
        progressDialog = new ProgressDialog(getContext());

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(emailField.getText().toString()) && !TextUtils.isEmpty(passwordField.getText().toString())){
                    String email = emailField.getText().toString();
                    String password = passwordField.getText().toString();
                    login(email,password);
                }else
                    Toast.makeText(getContext(), "All Fields are required", Toast.LENGTH_SHORT).show();

            }
        });
        return view;
    }

    private void login(String email, String password) {
        progressDialog.setMessage("Logging In");
        progressDialog.show();
        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if(task.isSuccessful()){
                            Toast.makeText(getContext(), "Welcome Back", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().finish();
                        }else{
                            Toast.makeText(getActivity(),
                                    "Incorrect Username/Password", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });
    }

}