package com.appsofsoumyadeep.notepad.RegisterScreen;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appsofsoumyadeep.notepad.MainActivity.MainActivity;
import com.appsofsoumyadeep.notepad.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CreateAccount extends Fragment {

    private EditText inputName, inputAge, inputEmail, inputPassword;
    private TextView inputDOB;
    private Button logIn, register;
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog progressDialog;
    private FirebaseUser user;

    public CreateAccount() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_create_account, container, false);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("Users");

        mAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(getContext());

        inputName = (EditText) view.findViewById(R.id.inputName);
        inputDOB = (TextView) view.findViewById(R.id.inputDOB);
        inputAge = (EditText) view.findViewById(R.id.inputAge);
        inputEmail = (EditText) view.findViewById(R.id.inputEmail);
        inputPassword = (EditText) view.findViewById(R.id.inputPassword);
        logIn = (Button) view.findViewById(R.id.loginPage);
        register = (Button) view.findViewById(R.id.register);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if(user!=null) {
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                }
            }
        };

        inputDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DOBPicker();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewAcc();
            }
        });

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_createAccount_to_loginScreen);
            }
        });

        mAuth.addAuthStateListener(mAuthListener);

        return view;
    }

    private void DOBPicker(){
        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month+=1;
                String date = day+"/"+month+"/"+year;
                inputDOB.setText(date);
            }
        },year, month, day);
        datePickerDialog.show();
    }

    private void createNewAcc() {
        String uName = inputName.getText().toString().trim();
        String uAge = inputAge.getText().toString().trim();
        String uDOB = inputDOB.getText().toString().trim();
        String mail = inputEmail.getText().toString().trim();
        String pwd = inputPassword.getText().toString().trim();

        if(!TextUtils.isEmpty(uName) && !TextUtils.isEmpty(uAge) &&
                !TextUtils.isEmpty(uDOB) && !TextUtils.isEmpty(mail) && !TextUtils.isEmpty(pwd)){

            if(pwd.length()<6)
                Toast.makeText(getContext(), "Please Enter a Valid Password", Toast.LENGTH_SHORT).show();
            else if(!mail.contains("@"))
                Toast.makeText(getContext(), "Please Enter a Valid Email", Toast.LENGTH_SHORT).show();
            else{
                progressDialog.setMessage("Creating Account");
                progressDialog.show();
                mAuth.createUserWithEmailAndPassword(mail,pwd).addOnSuccessListener(new OnSuccessListener<AuthResult>(){
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        if(authResult!=null){
                            String userID = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
                            DatabaseReference currentUserDB = databaseReference.child(userID);
                            Map<String,Object> userData = new HashMap<>();
                            userData.put("userName",uName);
                            userData.put("userAge",uAge);
                            userData.put("userDOB",uDOB);
                            userData.put("userID",userID);
                            currentUserDB.setValue(userData);
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), "Welcome to To-Do App", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getContext(), MainActivity.class));
                            getActivity().finish();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(getContext(), "Failed to Create Account." +
                                " Contact Developer!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
            }
        }
        else
            Toast.makeText(getContext(), "All Fields are required", Toast.LENGTH_SHORT).show();
        
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mAuthListener !=null)
            mAuth.removeAuthStateListener(mAuthListener);
    }
}