package com.appsofsoumyadeep.notepad.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.appsofsoumyadeep.notepad.R;
import com.appsofsoumyadeep.notepad.Model.Task;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TaskRecyclerViewAdapter extends RecyclerView.Adapter<TaskRecyclerViewAdapter.ViewHolder> {

    List<Task> taskList;
    Context context;

    public TaskRecyclerViewAdapter(Context context, List<Task> taskList) {
        this.context = context;
        this.taskList = taskList;
    }

    @NotNull
    @Override
    public TaskRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_row,parent,false);
        return new ViewHolder(view,context);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TaskRecyclerViewAdapter.ViewHolder holder, int position) {
        Task task = taskList.get(position);
        holder.taskTitle.setText(task.getTaskTitle());
        holder.taskDesc.setText("• "+task.getTaskDesc());
        holder.taskNum.setText(String.valueOf(position+1));
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView taskTitle, taskDesc, taskNum;
        public ViewHolder(@NonNull @NotNull View itemView, Context context) {
            super(itemView);
            taskTitle = (TextView) itemView.findViewById(R.id.taskTitle);
            taskDesc = (TextView) itemView.findViewById(R.id.taskDesc);
            taskNum = (TextView) itemView.findViewById(R.id.taskNumber);
        }
    }
}
