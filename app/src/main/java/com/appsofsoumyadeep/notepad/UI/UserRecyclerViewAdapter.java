package com.appsofsoumyadeep.notepad.UI;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.appsofsoumyadeep.notepad.R;
import com.appsofsoumyadeep.notepad.Model.User;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UserRecyclerViewAdapter extends RecyclerView.Adapter<UserRecyclerViewAdapter.ViewHolder> {

    List<User> userList;
    Context context;

    public UserRecyclerViewAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NotNull
    @Override
    public UserRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_row,parent,false);
        return new ViewHolder(view,context);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UserRecyclerViewAdapter.ViewHolder holder, int position) {
        User user = userList.get(position);
        holder.userName.setText("Name: " + user.getUserName());
        holder.userAge.setText("Age: "+user.getUserAge());
        holder.userDOB.setText("DOB: "+user.getUserDOB());
        holder.userImage.setText(String.valueOf(user.getUserName().charAt(0)));
    }

    @Override
    public int getItemCount() {
        return userList.size()  ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName, userAge, userDOB, userImage;
        public ViewHolder(@NonNull @NotNull View itemView, Context ctx) {
            super(itemView);
            context = ctx;
            userName = itemView.findViewById(R.id.userName);
            userAge = itemView.findViewById(R.id.userAge);
            userDOB = itemView.findViewById(R.id.userDOB);
            userImage = itemView.findViewById(R.id.userImage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = userList.get(getBindingAdapterPosition());
                    Bundle bundle = new Bundle();
                    bundle.putString("userID", user.getUserID());
                    Navigation.findNavController(v).navigate(R.id.action_userList_to_userTasks,bundle);
                }
            });
        }
    }
}
